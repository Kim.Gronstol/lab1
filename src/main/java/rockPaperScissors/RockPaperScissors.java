package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }


    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    boolean running = true;
    Random r = new Random();
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public void run() {
        while (running) {
            System.out.println("Let's play round " + String.valueOf(roundCounter));
            String input = readInput("Your choice (Rock/Paper/Scissors)?");
            while (!rpsChoices.contains(input)) {
                System.out.println("I don't understand " + input + ". Try again");
                input = readInput("Your choice (Rock/Paper/Scissors)?");
            }
            System.out.println(gameLogic(input));
            System.out.println("Score: human " + String.valueOf(computerScore) + ", computer " + String.valueOf(humanScore));
            String keepGoing = readInput("Do you wish to continue playing? (y/n)?");
            if ((keepGoing.equals("n") || keepGoing.equals("no"))) {
                running = false;
                System.out.println("Bye bye :)");
            }
        }
    }

    public String gameLogic(String input) {
        String computerChoice = computerChoice();

        if (((input.equals("rock")) & (computerChoice.equals("scissors"))) || ((input.equals("scissors")) & (computerChoice.equals("paper"))) || ((input.equals("paper")) & (computerChoice.equals("rock")))) {
            humanScore++;
            return "Human chose " + input + ", computer chose " + computerChoice + ". Human wins!";
        } else if (input.equals(computerChoice)) {
            return "Human chose " + input + ", computer chose " + computerChoice + ". It's a tie!";
        } else {
            computerScore++;
            return "Human chose " + input + ", computer chose " + computerChoice + ". Computer wins!";
        }
    }

    public String computerChoice() {
        return rpsChoices.get(r.nextInt(rpsChoices.size()));
    }

    /**
     * Reads input from console with given prompt
     *
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
